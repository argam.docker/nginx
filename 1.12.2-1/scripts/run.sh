#!/bin/sh

APP_NAME='base-nginx-1.12.2-1'

TAG='dev'

docker stop $APP_NAME || true

docker rm $APP_NAME || true

docker rmi $APP_NAME:$TAG || true

docker build -t $APP_NAME:$TAG .

